import { expect } from "chai";
import { ethers } from "hardhat";
import { beforeEach } from "mocha";
import { Contract } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";


describe("MyNFTTest", () => {
    let myNFTContract: Contract;
    let owner: SignerWithAddress;

    beforeEach(async () => {
        [owner] = await ethers.getSigners();
        
        const myNFTFactory = await ethers.getContractFactory("MyNFT");
        myNFTContract = await myNFTFactory.deploy();
    });

    it("Should be owner of myNFTContract", async () => {
        //when && then
        expect(await myNFTContract.owner()).to.equal(owner.address);
    });

    it("Should mint MyNFT", async () => {
        //given
        const tokenURI = "http://localhost:8080";

        //when && then
        await expect(myNFTContract.mintNFT(owner.address, tokenURI))
            .to.emit(myNFTContract, "Transfer")
            .withArgs(ethers.constants.AddressZero, owner.address, 1);
        expect(await myNFTContract.ownerOf(1)).to.equal(owner.address);
        expect(await myNFTContract.tokenURI(1)).to.equal("http://localhost:8080");
    });
});