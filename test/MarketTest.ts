import { expect } from "chai";
import { ethers, waffle } from "hardhat";
import { beforeEach } from "mocha";
import { Contract } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";


describe("MarketTest", () => {
	let myNFTContract: Contract;
	let marketContract: Contract;
	let seller: SignerWithAddress;
	let buyer1: SignerWithAddress;
	let buyer2: SignerWithAddress;

	beforeEach(async () => {
		[seller, buyer1, buyer2] = await ethers.getSigners();
		
		const marketFactory = await ethers.getContractFactory("Market");
		marketContract = await marketFactory.deploy();

		const myNFTFactory = await ethers.getContractFactory("MyNFT");
		myNFTContract = await myNFTFactory.deploy();
	});

	it("Should add item", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);

		//when
		await marketContract.addItem(myNFTContract.address, 1, 50)

		//then
		const item = await marketContract.itemsById(1);
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(false);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(marketContract.address);
	});

	it("Should not add item when price lower or equal to 0", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);

		//when && then
		await expect(marketContract.addItem(myNFTContract.address, 1, 0))
			.to.be.revertedWith("Price must be strictly greater than 0")
		const item = await marketContract.itemsById(1);
		expect(item.exists).to.equal(false);
		expect(await myNFTContract.ownerOf(1)).to.equal(seller.address);
	});

	it("Should buy item", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50)
		const sellerBalanceBeforeSale = await waffle.provider.getBalance(seller.address);

		//when 
		await marketContract.connect(buyer1).buyItem(1, { value: 50 });

		//then
		const item = await marketContract.itemsById(1);
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(true);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(buyer1.address);

		const sellerBalanceAfterSale = await waffle.provider.getBalance(seller.address);
		expect(sellerBalanceAfterSale.sub(sellerBalanceBeforeSale)).to.equal(50);
	});

	it("Should not buy item when itemId does not exist", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);

		//when && then
		await expect(marketContract.connect(buyer1).buyItem(1))
			.to.be.revertedWith("This itemId does not exist")
	});

	it("Should not buy item when value lower than price", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50)

		//when && then
		await expect(marketContract.connect(buyer1).buyItem(1))
			.to.be.revertedWith("Provided value is lower than requested price")
		const item = await marketContract.itemsById(1);
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(false);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(marketContract.address);
	});

	it("Should not buy item when already sold", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50)
		await marketContract.connect(buyer1).buyItem(1, { value: 50 });

		//when && then
		await expect(marketContract.connect(buyer2).buyItem(1, { value: 50 }))
			.to.be.revertedWith("Item has already been sold")
		const item = await marketContract.itemsById(1);
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(true);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(buyer1.address);
	});

	it("Should not buy own item", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50);

		//when && then
		await expect(marketContract.buyItem(1, { value: 50 }))
			.to.be.revertedWith("You can not buy your own item")
		const item = await marketContract.itemsById(1);
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(false);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(marketContract.address);
	});

	it("Should get items for sale", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50);

		//when 
		const items = await marketContract.getItemsForSale();

		//then
		expect(items.length).to.equal(1);
		const item = items[0];
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(false);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(marketContract.address);
	});

	it("Should get items for sale with only one sold item", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50);
		await marketContract.connect(buyer1).buyItem(1, { value: 50 });

		//when 
		const items = await marketContract.getItemsForSale();

		//then
		expect(items.length).to.equal(0);
	});

	it("Should get my items for sale", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50);

		//when 
		const items = await marketContract.getMyItemsForSale();

		//then
		expect(items.length).to.equal(1);
		const item = items[0];
		expect(item.id).to.equal(1);
		expect(item.contractAddress).to.equal(myNFTContract.address);
		expect(item.tokenId).to.equal(1);
		expect(item.seller).to.equal(seller.address);
		expect(item.price).to.equal(50);
		expect(item.isSold).to.equal(false);
		expect(item.exists).to.equal(true);
		expect(await myNFTContract.ownerOf(1)).to.equal(marketContract.address);
	});

	it("Should get my items for sale when only one sold item", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50);
		await marketContract.connect(buyer1).buyItem(1, { value: 50 });

		//when 
		const items = await marketContract.getMyItemsForSale();

		//then
		expect(items.length).to.equal(0);
	});

	it("Should get my items for sale when only one not owned item", async () => {
		//given
		await myNFTContract.mintNFT(seller.address, "http://localhost:8080")
		await myNFTContract.approve(marketContract.address, 1);
		await marketContract.addItem(myNFTContract.address, 1, 50);

		//when 
		const items = await marketContract.connect(buyer1).getMyItemsForSale();

		//then
		expect(items.length).to.equal(0);
	});
});