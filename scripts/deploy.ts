import { ethers } from "hardhat";

async function main() {
    const myNFTFactory = await ethers.getContractFactory("MyNFT");
    const myNFTContract = await myNFTFactory.deploy();
    console.log("MyNFT Contract deployed to address:", myNFTContract.address);

    const marketFactory = await ethers.getContractFactory("Market");
    const marketContract = await marketFactory.deploy();
    console.log("Market Contract deployed to address:", marketContract.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
