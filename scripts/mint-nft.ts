import { createAlchemyWeb3 } from "@alch/alchemy-web3";
import { AbiItem } from 'web3-utils'
import * as nftContractJson from "../artifacts/contracts/MyNFT.sol/MyNFT.json";
import * as dotenv from "dotenv";

dotenv.config();

const API_URL = process.env.API_URL as string;
const PUBLIC_KEY = process.env.PUBLIC_KEY as string;
const PRIVATE_KEY = process.env.PRIVATE_KEY as string;
const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS;

const web3 = createAlchemyWeb3(API_URL);

const nftContract = new web3.eth.Contract(
    nftContractJson.abi as AbiItem[],
    NFT_CONTRACT_ADDRESS,
);

async function mintNFT(tokenURI: string) {
    const nonce = await web3.eth.getTransactionCount(PUBLIC_KEY, 'latest');
    const data = nftContract.methods.mintNFT(PUBLIC_KEY, tokenURI).encodeABI()

    const tx = {
        'from': PUBLIC_KEY,
        'to': NFT_CONTRACT_ADDRESS,
        'nonce': nonce,
        'gas': 500000,
        'maxPriorityFeePerGas': 1999999987,
        'data': data
    };

    const signedTx = await web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);

    if (signedTx.rawTransaction != null) {
        const transactionReceipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        console.log(`Transaction receipt: ${JSON.stringify(transactionReceipt)}`);
    }

}

mintNFT("https://gateway.pinata.cloud/ipfs/QmYueiuRNmL4MiA2GwtVMm6ZagknXnSpQnB3z2gWbz36hP")
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
