# NFT Marketplace

## Summary

This is a training project to learn about Solidity and NFTs.

## Resources used

- https://github.com/dabit3/polygon-ethereum-nextjs-marketplace/
- https://docs.alchemy.com/alchemy/tutorials/how-to-create-an-nft/how-to-mint-a-nft
- https://docs.alchemy.com/alchemy/tutorials/how-to-create-an-nft