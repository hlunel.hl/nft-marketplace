//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";

contract Market is IERC721Receiver {
    using Counters for Counters.Counter;
    Counters.Counter private _itemIds;
    Counters.Counter private _itemsSold;

    mapping(uint256 => Item) public itemsById;

    struct Item {
        uint256 id;
        address contractAddress;
        uint256 tokenId;
        address payable seller;
        uint256 price;
        bool isSold;
        bool exists;
    }

    // IERC721Receiver implementation
    function onERC721Received(
        address,
        address,
        uint256,
        bytes memory
    ) public virtual override returns (bytes4) {
        return this.onERC721Received.selector;
    }

    // Place an item for sale
    function addItem(
        address itemContractAddress,
        uint256 itemTokenId,
        uint256 itemPrice
    ) public returns (uint256) {
        require(itemPrice > 0, "Price must be strictly greater than 0");

        IERC721(itemContractAddress).safeTransferFrom(
            msg.sender,
            address(this),
            itemTokenId
        );

        _itemIds.increment();
        uint256 itemId = _itemIds.current();

        itemsById[itemId] = Item(
            itemId,
            itemContractAddress,
            itemTokenId,
            payable(msg.sender),
            itemPrice,
            false,
            true
        );

        return itemId;
    }

    // Buy an item
    function buyItem(uint256 itemId) public payable {
        Item storage item = itemsById[itemId];
        require(item.exists, "This itemId does not exist");
        require(!item.isSold, "Item has already been sold");
        require(msg.sender != item.seller, "You can not buy your own item");
        require(
            msg.value == item.price,
            "Provided value is lower than requested price"
        );

        item.isSold = true;
        _itemsSold.increment();

        item.seller.transfer(msg.value);
        IERC721(item.contractAddress).safeTransferFrom(
            address(this),
            msg.sender,
            item.tokenId
        );
    }

    // Get all items for sale
    function getItemsForSale() public view returns (Item[] memory) {
        uint256 numberOfItems = _itemIds.current();
        uint256 numberOfSoldItems = _itemsSold.current();
        uint256 numberOfUnsoldItems = numberOfItems - numberOfSoldItems;
        uint256 currentIndex = 0;

        Item[] memory items = new Item[](numberOfUnsoldItems);

        for (uint256 itemId = 0; itemId < numberOfItems; itemId++) {
            uint256 currentId = itemId + 1;
            Item storage currentItem = itemsById[currentId];
            if (currentItem.exists && !currentItem.isSold) {
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }

        if (currentIndex == 0) return new Item[](0);
        return items;
    }

    // Get all items you are currently selling
    function getMyItemsForSale() public view returns (Item[] memory) {
        uint256 numberOfItems = _itemIds.current();
        uint256 numberOfSoldItems = _itemsSold.current();
        uint256 numberOfUnsoldItems = numberOfItems - numberOfSoldItems;
        uint256 currentIndex = 0;

        Item[] memory items = new Item[](numberOfUnsoldItems);

        for (uint256 itemId = 0; itemId < numberOfItems; itemId++) {
            uint256 currentId = itemId + 1;
            Item storage currentItem = itemsById[currentId];
            if (
                currentItem.exists &&
                !currentItem.isSold &&
                currentItem.seller == msg.sender
            ) {
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }

        if (currentIndex == 0) return new Item[](0);
        return items;
    }
}
